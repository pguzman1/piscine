/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/15 12:03:55 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/15 16:53:27 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

int		ft_str_is_printable(char *str)
{
	int i;
	int length;

	length = ft_strlen(str);
	i = 0;
	if (str != '\0')
	{
		while (str[i] != '\0' && str[i + 1] != '\0')
		{
			if (str[i] < 32)
			{
				return (0);
			}
			i++;
		}
		return (1);
	}
	return (1);
}
