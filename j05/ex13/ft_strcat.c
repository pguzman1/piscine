/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/15 16:55:26 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/15 19:56:17 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

char	*ft_strcat(char *dest, char *src)
{
	int length;
	int i;

	i = 0;
	length = ft_strlen(dest);
	while ( i < ft_strlen (dest) + ft_strlen(src))
	{
		dest[length + 1 + i] = src[i];
	}
	dest[length + ft_strlen(src)] = '\0';
	return (dest);
}
