/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/15 09:21:04 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/27 17:29:40 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_number_letter(char letter)
{
	if ('9' >= letter && letter >= '0')
	{
		return (1);
	}
	if (letter >= 'A' && letter <= 'Z')
	{
		return (1);
	}
	if (letter <= 'z' && letter >= 'a')
	{
		return (1);
	}
	else
	{
		return (0);
	}
}

char	*ft_strcapitalize(char *str)
{
	int counter;

	counter = 0;
	while (str[counter] != '\0')
	{
		if (str[counter] >= 'A' && str[counter] <= 'Z' && counter != 0)
		{
			if (ft_is_number_letter(str[counter - 1]) == 1)
			{
				str[counter] = str[counter] + 32;
			}
		}
		if (str[counter] >= 'a' && str[counter] <= 'z' && counter != 0)
		{
			if (ft_is_number_letter(str[counter - 1]) == 0)
			{
				str[counter] = str[counter] - 32;
			}
		}
		counter++;
	}
	return (str);
}
