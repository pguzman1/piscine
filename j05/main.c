/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/11 10:26:08 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/15 23:06:08 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>

int		ft_strlen(char *str);
int		ft_sqrt(int index);
int		ft_is_prime(int nb);
int		ft_iterative_factorial(int nb);
int		ft_sqrt(int nb);
void	ft_putchar(char a);
int		ft_putnbr(int a);
int		ft_find_next_prime(int nb);
void	ft_putstr(char *str);
char	*test(void);
char	*ft_strstr(char *str, char *to_find);
char	*ft_strcpy(char *dest, char *src);
char	*ft_strncpy(char *dest, char *src, unsigned int a);
int		ft_strcmp(char *a, char *b);
int		ft_strncmp(char *a, char *b, int n);
char	*ft_strupcase(char *str);
char	*ft_strlowcase(char *str);
char	*ft_strcapitalize(char *str);
int		ft_str_is_alpha(char *str);
int		ft_str_is_numeric(char *str);
int		ft_str_is_lowcase(char *str);
int		ft_str_is_uppercase(char *str);
int		ft_str_is_printable(char *str);
char	*ft_strcat(char *a, char *b);
int		main(void)
{
	char a[] = "";
	char b[] = "aaaaaaaa";
	
	char *c = ft_strstr(b, a);
	char *d = strstr(b,a);
//	ft_putstr(a);
//	ft_putchar('\n');
//	char c = strcmp(b, a);

//	char *c = strncpy(b,a,6);
//	char *d = ft_strncpy(b,a,6);
	printf("%s\n", c);
	printf("%s\n", d);
//	ft_putnbr(d);
//	ft_putstr(c);
	
	return (0);
}
