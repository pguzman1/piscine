/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_foreach.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/21 13:24:45 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/21 22:56:28 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putnbr(int n);

void ft_foreach(int *tab, int length, void(*f)(int))
{
	int i;

	i = 0;
	while (i < length)
	{
		f(tab[i]);
		i++;
	}
}

int		main(void)
{
	int a[2];
	a[0] = 1;
	a[1] = 2;
	void (*f)(int);
	f = &ft_putnbr;
	ft_foreach(a, 2, f);
}
