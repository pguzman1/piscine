/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/21 18:01:59 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/22 20:57:30 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
int		atoi(char *str)
{
	int res;
	int i;

	i = 0;
	while (str[i])
	{
		res = (res * 10) + str[i] - '0';
		i++;
	}
	return (res);
}

int ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

int		ft_isnumber(char c)
{
	if (c >= '0' && c <= '9')
		return (1);
	else
		return (0);
}


int		ft_getnumbers(char *c)
{
	int i;

	char number[99999];
	i = 0;
	if (c[0] == '-')
	{
		i = 1;
	}
	while (ft_isnumber(c[i]) == 1)
	{	
		i++;
	}
	if (i > 0)
	{
		while (i > 0)
		{
			number[i] = c[i];
			i--;
		}
		return (atoi(number));
	}
	return (0);
}


int		ft_getoperator(char *operator)
{
	if (operator[2] == '\0')
		return (1);
	else
		return (0);
}

char *ft_itoa(int n)
{
	char *s;
	int i;

	s = "";
	s = ;
	*s = 0;
	while (s > 0)
	{
		s = s + "";
		s = n % 10 + '0';
		n = n / 10;
		s--;
		if (n = 0)
			break;
	}
	return (s);
}


int		main(int argc, char **argv)
{
	char *c;
	if (argc == 4)
	{
		if (argv[2][1] == '\0')
		{
			if (argv[2][0] == '+')
			{
				c = ft_itoa(ft_getnumbers(argv[1]) + ft_getnumbers(argv[3]));
				write(1, &c,ft_strlen(c));
			}
			else if (argv[2][0] == '-')
				return (ft_getnumbers(argv[1]) - ft_getnumbers(argv[3]));
			else if (argv[2][0] == '*')
				return (ft_getnumbers(argv[1]) * ft_getnumbers(argv[3]));
			else if (argv[2][0] == '/')
			{
				if (ft_getnumbers(argv[3]) == 0)
					write(1,"Stop : division by zero\n", 24);
				else
					return (ft_getnumbers(argv[1]) / ft_getnumbers(argv[3]));
			}
			else if (argv[2][0] == '%')	
			{
				if (ft_getnumbers(argv[3]) == 0)
					write(1,"Stop : module by zero\n", 22);
				else
					return (ft_getnumbers(argv[1]) % ft_getnumbers(argv[3]));
			}
		}
		else
			write(1, "0", 1);
	}
	else
		return (0);
}

