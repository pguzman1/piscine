/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/21 16:09:46 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/23 18:38:18 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_is_sort(int *tab, int length, int (*f)(int, int))
{
	int i;
	int sort;
	int sort_rev;

	i = 0;
	sort = 0;
	sort_rev = 0;
	while (i < length - 1)
	{
		if (f(tab[i], tab[i + 1]) > 0)
		{
			sort = 1;
		}
		else if (f(tab[i], tab[i + 1]) < 0)
		{
			sort_rev = 1;
		}
		i++;
	}
	if (sort == 1 && sort_rev == 1)
		return (0);
	return (1);
}

int		ft(a, b)
{
	return (a - b);
}

int		main(void)
{
	int a[4] = {1, 1, 1, 1};
	int (*f)(int, int);
	f = &ft;

	printf("%d\n", ft_is_sort(a, 4, f));
}
