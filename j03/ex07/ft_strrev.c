/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/11 17:56:05 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/12 11:25:37 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strrev(char *str)
{
	int		string_length;
	int		counter;
	char	temporary;

	string_length = 0;
	counter = 0;
	while (str[string_length])
	{
		string_length++;
	}
	while (counter < string_length - 1)
	{
		temporary = str[string_length - 1];
		str[string_length - 1] = str[counter];
		str[counter] = temporary;
		counter++;
		string_length--;
	}
	return (0);
}
