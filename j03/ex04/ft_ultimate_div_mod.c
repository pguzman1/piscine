/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_div_mod.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/11 14:58:19 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/11 16:01:05 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void ft_ultimate_div_mod(int *a, int *b)
{
	int transitory1;
	int transitory2;

	transitory1 = *a;
	transitory2 = *b;
	*a = transitory1 / transitory2;
	*b = transitory1 % transitory2;
}
