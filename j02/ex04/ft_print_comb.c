/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/09 18:50:42 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/10 23:39:21 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_put_number(char a, char b, char c)
{
	ft_putchar(a);
	ft_putchar(b);
	ft_putchar(c);
	if (a != '7')
	{
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void	ft_print_comb(void)
{
	char numbers[3];

	numbers[0] = '0';
	numbers[1] = '0';
	numbers[2] = '0';
	while (numbers[0] != '8')
	{
		numbers[2]++;
		if (numbers[0] < numbers[1] && numbers[1] < numbers[2])
		{
			ft_put_number(numbers[0], numbers[1], numbers[2]);
		}
		if (numbers[2] == '9')
		{
			numbers[2] = '0';
			numbers[1]++;
		}
		if (numbers[1] == '9')
		{
			numbers[1] = '0';
			numbers[0]++;
		}
	}
}
