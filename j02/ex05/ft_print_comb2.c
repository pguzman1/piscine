/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/10 09:35:08 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/10 15:32:51 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_putchar.h"

void ft_print_numbers(char firstn, char secondn, char thirdn, char forthn)
{
			ft_putchar(firstn);
			ft_putchar(secondn);
			ft_putchar(thirdn);
			ft_putchar(forthn);
}

//void ft_count_numbers(char firstn, char secondn, char thirdn, char forthn)
//{
//	forthn++;
//	if (forthn == '9')
//	{
//		thirdn++;
//		forthn = '0';
//	}
//	if (thirdn == '9' && forthn == '9')
//	{
//		secondn++;
//		thirdn = '0';
//		forthn = '0';
//	}
//	if (secondn == '9')
//	{
//		firstn++;
//		secondn = '0';
//	}
//}

void ft_print_comb2(void)
{
	char numbers[4];

	numbers[0]='0';
	numbers[1]='0';
	numbers[2]='0';
	numbers[3]='1';
	while (numbers[0] != '9')
	{
	//	ft_count_numbers(numbers[0], numbers[1], numbers[2], numbers[3]);
	ft_print_numbers(numbers[0], numbers[1], numbers[2], numbers[3]);
	if (numbers[3] == '9')
	{
		numbers[2]++;
		numbers[3] = '0';
	}
	if (numbers[2] == '9')
	{
		numbers[1]++;
		numbers[2] = '0';

	}
	if (numbers[1] == '9')
	{
		numbers[0]++;
		numbers[1] = '0';
	}
	numbers[3]++;
	}
}

int		main(void)
{
	ft_print_comb2();
	return (0);
}
