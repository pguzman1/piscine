/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colle01.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: flefebvr <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/12 10:54:54 by flefebvr          #+#    #+#             */
/*   Updated: 2015/07/26 19:19:42 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void ft_putchar(char c);

void ft_write_line(int x, int y, int line, int colum)
{
	if (line == 1 || line == y)
	{
		if ((colum == 1 && line == 1) ||
				(colum == x && line == y && line != 1 && colum != 1))
			ft_putchar('/');
		else if (colum == x || (colum == 1 && line == y))
			ft_putchar('\\');
		else
			ft_putchar('*');
	}
	else
	{
		if (colum == 1 || colum == x)
			ft_putchar('*');
		else
			ft_putchar(' ');
	}
}

void ft_colle01(int x, int y)
{
	int line;
	int colum;

	line = 1;
	while (line <= y)
	{
		colum = 1;
		while (colum <= x)
		{
			ft_write_line(x, y, line, colum);
			colum++;
		}
		if(x != 0)
			ft_putchar('\n');
		line++;
	}
}
