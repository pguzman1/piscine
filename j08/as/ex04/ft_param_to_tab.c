/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_param_to_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/20 11:05:35 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/21 19:47:26 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_par.h"
#include <stdlib.h>

int					ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}

char				*ft_strdup(char *src)
{
	char	*copy;
	int		i;

	i = 0;
	copy = malloc(ft_strlen(src) + 1);
	while (src[i])
	{
		copy[i] = src[i];
		i++;
	}
	copy[i] = '\0';
	return (copy);
}

struct s_stock_par	*ft_param_to_tab(int ac, char **av)
{
	int					i;
	int					k;
	struct s_stock_par	*array;

	i = 0;
	array = (struct s_stock_par *)malloc((ac + 1) * sizeof(*array));
	while (i < ac)
	{
		array[i].str = av[i];
		array[i].size_param = ft_strlen(av[i]);
		array[i].copy = ft_strdup(av[i]);
		k = 0;
		array[i].tab = (char **)malloc(sizeof(char *) * ft_strlen(b[i]));
		while (k < 9)
		{
			array[i].tab[k] = (char*)malloc(ft_strlen(b[i][k]) * sizeof(char));
			k++;
		}
		array[i].tab = ft_split_whitespaces(av[i]);
		i++;
	}
	array[i].str = 0;
	return (array);
}
