/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/20 10:47:28 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/20 11:35:46 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	**ft_split_whitespaces(char *str)
{
	int i;

	while (str[i])
	{
		if (str[i] =='\n' || str[i] == ' ' || str[i] == '\t')
		{
		
			ft_split_whitespaces(str + 1);	
		}
		i++;
	}
}
