/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hoa.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/22 18:58:29 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/22 19:48:24 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
char *ft_itoa(int n)
{
	char *s;

	s = malloc(999);
	s = s + 999;
	*s = 0;
	while (s > 0)
	{
		s--;
		*s = n % 10 + '0';
		n = n / 10;
		if (n == 0)
			break;
	}
	return (s);
}

int		main(void)
{
	char *str;
		
	str = ft_itoa(998);
	write(1, str, 3);
	
}
