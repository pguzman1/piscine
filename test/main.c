/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/11 10:26:08 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/19 17:16:05 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str);
int		ft_sqrt(int index);
int		ft_is_prime(int nb);
int		ft_iterative_factorial(int nb);
int		ft_sqrt(int nb);
void	ft_putchar(char a);
int		ft_putnbr(int a);
int		ft_find_next_prime(int nb);
void	*ft_putstr(char *str);
char	*test(void);
int		main(void)
{
	char *a;
	a = test();
	ft_putchar(a[0]);
	ft_putchar(a[1]);
	return (0);
}
