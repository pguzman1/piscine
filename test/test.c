/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   iBy: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/14 08:51:01 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/14 09:35:02 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_putstr(char *str);

char *test(void)
{
	char a[] = "hola";
	char b[] = "peroo";
	ft_putstr(strcpy(a,b));
	return (strcpy(a, b));
}
