/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lol.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/23 21:50:25 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/24 08:26:02 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int		main(void)
{
	char a = 'a';
	char array[20];
	int b = (int)a;
	int i = 0;
	printf("%d\n", b);
	while (b > 0)
	{
		array[i] =(char)(b % 2);
		b = b / 2;
		i++;
	}
	i = 0;
	while (i < 7)
	{
		if (array[i] == 0)
			printf("%c", '0');
		else if(array[i] == 1)
			printf("%c", '1');
		i++; 
	}
}
