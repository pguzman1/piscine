/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_apply_prefix.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/24 10:40:07 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/25 22:12:33 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void	btree_apply_prefix(t_btree *root, void (*applyf)(void *))
{
	t_btree *tree;

	tree = root;
	while (tree)
	{
		applyf(tree);
		if (tree->left)
			btree_apply_prefix(tree->left, (*applyf)(void *));
		if (tree->rigth)
			btree_apply_prefix(tree->rigth, (*applyf)(void *));
	}
}
