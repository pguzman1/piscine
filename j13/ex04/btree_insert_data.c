/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_insert_data.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/25 11:15:49 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/25 22:28:22 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

void btree_insert_data(t_btree **root, void *item, int (*cmpf)(void *, void *))
{
	t_btree *tree;

	tree = *root;
	while (tree)
	{
		if (cmpf(item, tree->item) < 0)
		{
			tree = tree->left;
			if (!tree)
			{
				tree = btree_create_node(item);
				break ;
			}
		}
		else
		{
			tree = tree->right;
			if (!tree)
			{
				tree = btree_create_node(item);
				break ;
			}
		}
	}
	tree = btree_create_node(item);
}
