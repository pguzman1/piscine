/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_apply_suffix.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/25 10:30:06 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/25 22:15:52 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	btree_apply_suffix(t_btree *root, void (*applyf)(void *))
{
	t_btree *tree;

	if (tree)
	{
		if (tree->left)
			btree_apply_prefix(tree->left, (*applyf)(void *));
		if (tree->rigth)
			btree_apply_prefix(tree->rigth, (*applyf)(void *));
		applyf(tree);
	}
}
