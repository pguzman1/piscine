/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_apply_infix.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/25 08:48:44 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/25 20:25:18 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	btree_apply_infix(t_btree *root, void (*applyf)(void *))
{
	t_btree *tree;

	if (tree)
	{
		if (tree->left)
			btree_apply_prefix(tree->left, (*applyf)(void *));
		applyf(tree);
		if (tree->rigth)
			btree_apply_prefix(tree->rigth, (*applyf)(void *));
	}
}
