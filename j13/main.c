/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/25 17:16:29 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/25 22:56:49 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"
#include <unistd.h>
t_btree *btree_create_node(void *item);
void btree_apply_prefix(t_btree *root, void (*applyf)(void *));
void btree_insert_data(t_btree **root, void *item, int (*cmpf)(void *, void *));

void ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int n)
{
	if (n < 0)
	{
		ft_putchar('-');
		n = -n;
	}
	if (n > 9)
		ft_putnbr(n / 10);
	ft_putchar(48 + n % 10);
}

int		cmpf(int *a, int *b)
{
	return (a - b);
}
int		main(void)
{
	int		a = 9;
	int 	b = 8;
	int		*ptr;
	int		*ptr1;

	ptr1 = b
	ptr = a;
	t_btree	*root;
	int (*cmpf)(void*, void*);
	root = btree_create_node(a);
	btree_insert_data(&root, b, (cmpf));
	return (0);
}
