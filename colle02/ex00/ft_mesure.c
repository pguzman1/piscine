/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mesure.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/25 16:17:35 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/25 16:52:58 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int ft_length(char *buf)
{
	int i;

	i = 0;
	while (buf[i] != '\n')
		i++;
	return (i);
}

int ft_width(char *buf)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (buf[i] != '\0')
	{
		if (buf[i] = '\n')
			j++;
		i++;
	}
	j--;
	return (j);
}

void	ft_print
