/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_bits.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tgascoin <tgascoin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/30 10:08:57 by tgascoin          #+#    #+#             */
/*   Updated: 2015/07/30 13:22:09 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

void    ft_putchar(char c)
{
    write(1, &c, 1);
}

void    print_bits(unsigned char octet)
{
    char    temp[9];
    int     i;
    int     j;

    i = 0;
    while (octet > 0)
        temp[i++] = (octet % 2 == 0) ? '0' : '1', octet /= 2;
    temp[i] = '\0';
    j = i;
    while (j <= 7)
        ft_putchar('0'), j++;
    while (i >= 0)
        ft_putchar(temp[i--]);
    ft_putchar('\n');
}

int     main(void)
{
    unsigned char octet;

    octet = 14;
    print_bits(octet);
    return (0);
}
