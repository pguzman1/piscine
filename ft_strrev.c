/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strreV.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/31 16:27:36 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/31 17:01:49 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

char *ft_strrev(char *str)
{
	char	temp;
	int		length;
	int 	i;

	length = 0;
	i = 0;
	while (str[length])
		length++;
	str[length] = '\0';
	while (i < (length / 2))
	{
		temp = str[i];
		str[i] = str[length - 1 - i];
		str[length - 1 - i] = temp;
		i++;
	}
	return (str);
}

int main(void)
{
	char *asdf;
	char *str;
	char a[] = "hola";
	asdf = a;
	str = ft_strrev(asdf);
	printf("%s\n", str);
}
