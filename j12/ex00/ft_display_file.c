/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/23 11:24:50 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/28 11:41:41 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#define BUF_SIZE 5000
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

void	ft_putnbr(int n)
{
	if (n < 0)
	{
		ft_putchar('-');
		n = -n;
	}
	if (n > 9)
		ft_putnbr(n / 10);
	ft_putchar(48 + n % 10);
}

int		main(int argc, char **argv)
{
	int		ret;
	char	buf[BUF_SIZE + 1];
	int		i;
	char	test = 'h';

	i = 0;
		write(1,&test, 1);
	while ((ret = read(0, buf, BUF_SIZE)))
	{
		buf[ret] = '\0';
		if (buf[0] == 'h')
			ft_putstr("colle00");
		else if (buf[0] == '/' || buf[0] =='\\')
			ft_putstr("colle01");
	//	ft_putstr(buf);
	}
}
