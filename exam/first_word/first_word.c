/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   first_word.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/27 12:34:38 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/27 16:09:53 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		main(int argc, char ** argv)
{
	int i;

	i = 0;
	while (is_good(argv[i] == 0))
		i++;
	while (is_good(argv[i]) == 1)
	{
		ft_putchar(argv[i]);
		i++;
	}
}

int		is_good(char c)
{
	if (c != ' ' && c != '\v' && c != '\t')
	{
		return (1);
	}
	else
		return (0);
}
