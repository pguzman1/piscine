/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   epur_str.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/29 10:28:34 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/29 13:31:03 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		main(int argc, char **argv)
{
	int i;

	i = 0;
	while(argv[1][i] == ' ')
		i++;
	while (argv[1][i])
	{
		if (!(argv[1][i] == ' ' && argv[1][i - 1] == ' '))
		{
			ft_putchar(argv[1][i]);
		}
		i++;
	}
	return (0);
}
