/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap_bits.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/29 08:42:16 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 15:29:20 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

unsigned char	swap_bits(unsigned char octet)
{
	return (octet);
}

int				main(void)
{
	unsigned char a = 97;
	unsigned char b;
	b = (a << 4) + (a >> 4);
	printf("%d", b);
	
}
