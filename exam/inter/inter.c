/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inter.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/27 18:23:18 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/28 09:20:27 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int 	is_al(int i, char c, char *ca)
{
	int a;

	a = 0;
	while (ca[a])
	{
		if (ca[a] == c)
		{
			if (i == a)
			{
				return (1);
			}
			return (0);
		}			
		a++;
	}
	return (0);	
}
int		main(int argc, char **argv)
{
	int i;
	int j;
	int start;

	i = 0;
	j = 0;
	start = 0;
	while (argv[1][i])
	{
		j = 0;
		while (argv[2][j])
		{
			if (argv[1][i] == argv[2][j])
			{
				if (is_al(i, argv[1][i], argv[1]))
				ft_putchar(argv[1][i]);
				start = j + 1;
				break;
			}
			j++;
		}
		i++;
	}
	return (0);
}

