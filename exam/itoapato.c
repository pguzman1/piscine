/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoapato.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/26 11:05:12 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 17:10:35 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

char	*ft_strrev(char *str);

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while(str[i])
	{
		i++;
	}
	return (i);
}


char *ft_itoa(int n, char *str)
{
	int res;
	int i;
	i = 0;
	res = 0;
	while (n != 0)
	{
		if (n > 9)
		{
			res = n % 10;
			str[i] = res + '0';
			n = n / 10;
		}
		else if (n < 10)
		{
			str[i] = n + '0';
			n = n / 10;
		}
		i++;
	}
	str[i] = '\0';

	return (str);
}

int		main(void)
{
	int a;
	char str[4];

	str[0] = 0;
	str[1] = 1;
	str[2] = 3;
	str[3] = 4;

	a = 128;
	char *b = ft_itoa(a, str);
	char *c = ft_strrev(b);
	ft_putstr(c);
	return (0);
}
