/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   maff_alpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/27 16:50:26 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/27 17:21:14 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &(c), 1);
}

void	maff_alpha(void)
{
	char c = 'a';
	while (c <= 'z')
	{
		ft_putchar(c);
		ft_putchar((c + 1) - 32);
		c = c + 2;
	}
}

int main(void)
{
	maff_alpha();
	return (0);
}
