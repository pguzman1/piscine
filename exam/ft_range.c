/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/16 13:59:41 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 10:42:38 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	int	i;
	int	*array;
	int temp;

	temp = max - min;
	i = 0;
	if (temp <= 0)
	{
		return (0);
	}
	array = (int*)malloc(sizeof(*array) * temp + 1);
	while (i < temp)
	{
		array[i] = min + i;
		i++;
	}
	array[i] = '\0';
	return (array);
}
