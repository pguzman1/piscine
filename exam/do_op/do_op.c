/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do_op.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/29 15:41:23 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 15:08:02 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int		main(int argc, char **argv)
{
	if (argv[2][0] == '+')
		printf("%d\n", atoi(argv[1]) + atoi(argv[3]));
	if (argv[2][0] == '-')
		printf("%d\n", atoi(argv[1]) - atoi(argv[3]));
	if (argv[2][0] == '/')
		printf("%d\n", atoi(argv[1]) / atoi(argv[3]));
	if (argv[2][0] == '*')
		printf("%d\n", atoi(argv[1]) * atoi(argv[3]));
	if (argv[2][0] == '%')
		printf("%d\n", atoi(argv[1]) % atoi(argv[3]));
}
