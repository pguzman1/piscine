/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   putput.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/30 14:54:15 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 15:05:57 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int n)
{
	char c;

	
	if (n > 9)
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
	else if
	{
		c = n + '0';
		ft_putchar(c);
	}
	else
	{
		n = -n;
		ft_putchar('-');
		ft_putnbr(n);	
	}
}
