/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tab_mult.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/29 11:11:45 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/29 11:38:22 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void		ft_putstr(char *str)
{
	int		i;
	char	c;

	i = 0;
	while (str[i])
	{
		c = str[i];
		write(1, &c, 1);
		i++;
	}
}

void		ft_putnbr(int nbr)
{
	int		num;
	char	n;

	num = 0;
	if (nbr > 9)
	{
		num = nbr / 10;
		ft_putnbr(num);
		nbr = nbr % 10;
	}
	n = 48 + nbr;
	write(1, &n, 1);
}

void		ft_print(int nbr)
{
	int		i;
	char	num;
	int		mult;
	char	tab;

	tab = '\n';
	i = 1;
	while (i < 10)
	{
		ft_putnbr(i);
		ft_putstr(" x ");
		ft_putnbr(nbr);
		ft_putstr(" = ");
		mult = i * nbr;
		ft_putnbr(mult);
		write(1, &tab, 1);
		i++;
	}
}

int			ft_atoi(char *str)
{
	int	i;
	int	res;

	res = 0;
	i = 0;
	while (str[i])
	{
		res = (res * 10) + str[i] - '0';
		i++;
	}
	return (res);
}

int			main(int argc, char **argv)
{
	int		num;
	char	tab;

	tab = '\n';
	if (argc == 1)
	{
		write(1, &tab, 1);
		return (0);
	}
	num = ft_atoi(argv[1]);
	ft_print(num);
	return (0);
}

