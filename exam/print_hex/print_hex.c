/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_hex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/29 10:54:47 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/29 14:16:23 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

char	lettre(int i)
{
	char c;
	if (i % 16 == 15)
		return ('F');
	else if (i % 16 == 14)
		return ('E');
	else if (i % 16 == 13)
		return ('D');
	else if (i % 16 == 12)
		return ('C');
	else if (i % 16 == 11)
		return ('B');
	else if (i % 16 == 10)
		return ('A');
	else
	{
		c = (i % 16) + '0';
		return (c);
	}
}




void	change(int i)
{
	char c;
	int h;

	while (i != 0)
	{
		c = lettre(i);
		ft_putchar(c);
		i = i / 16;
	}
}

int		ft_atoi(char *str)
{
	int i;
	int res = 0;

	i = 0;
	res = 0;
	while (str[i])
	{
		if (str[i] >= '0' && str[i] <= '9')
			res = (res * 10) + str[i] - '0';
		else
			return (-1);	
		i++;
	}
	return (res);
}

int		main(int argc, char **argv)
{
	int num;

	num = ft_atoi(argv[1]);
	printf("%d\n", num);
	change(num);
	return (0);
}

