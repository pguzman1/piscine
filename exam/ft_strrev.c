/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/26 11:28:40 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/31 17:02:43 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		i++;
	}
	return (i);
}
char	*ft_strrev(char *str)
{
	int i;
	char temp;
	int length;

	temp = 'a';
	length = ft_strlen(str);
	i = 0;
	while (i < (length / 2))
	{
		temp = str[i];
		str[i] = str[length - i - 1];
		str[length -i -1] = temp;
		i++;
	}
	str[length] = '\0';
	return (str);
}

 int     main(void)
 {
     char chaine[] = "saluta";
 
     printf("%s", ft_strrev(chaine));
     return (0);
 }
