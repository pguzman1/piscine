/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   itoa.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/29 17:25:44 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/31 16:16:08 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

char	*ft_itoa(int nbr)
{
	char	*str;
	int		temp;
	int		i;
	int		n;

	i = 1;
	n = 0;
	if (nbr < 0)
	{
		nbr = -nbr;
		i++;
		n = 1;
	}
	temp = nbr;
	while (temp > 10)
		i++, temp = temp / 10;
	str = (char *)malloc((sizeof(char) * (i + 1)));

	while (i > 0)
	{
		str[i - 1] = (nbr % 10) + '0'; 
		nbr = nbr / 10;
		i--;
	}
	str[temp + 1] = '\0';
	if (n == 1)
		str[0] = '-';
	return (str);
}

int		main(void)
{
	char *str;
	char *str1;
	str1 = itoa(3);
	str = ft_itoa(3);
	printf("%s\n", str);
	printf("%s\n", str1);
}
