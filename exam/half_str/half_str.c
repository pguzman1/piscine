/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   half_str.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/30 10:14:39 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 10:31:46 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		main(int argc, char **argv)
{
	int i;
	int j;

	i = 1;
	j = 0;
	if (argc == 1)
	{
		ft_putchar('\n');
		return (0);
	}
	while (argv[i])
	{
		j = 0;
		while (argv[i][j])
		{
			if (j % 2 == 0)
			{
				ft_putchar(argv[i][j]);
			}
			j++;
		}
		ft_putchar('\n');

		i++;
	}
	return (0);
}
