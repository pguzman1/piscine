/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_bits.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/27 10:41:21 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/31 13:53:18 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>
void	ft_putchar(char a)
{
	write(1, &a, 1);
}

void ft_putnbr(int n)
{
	if (n >= 10)
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
	else
		ft_putchar(n + '0');
}

void	print_bits(unsigned char octet)
{
	int array[8];
	int nbr = octet;
	int i = 0;

	while (nbr != 0)
	{
		array[i] = (nbr % 2);
		nbr = nbr / 2;
		i++;
	}
	i = 7;
	while (i > 0)
	{
		ft_putnbr(array[i]);
		i--;
	}
}

int main(void)
{
	unsigned char a = 148;
	print_bits(a);
	return (0);
}
