/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/29 16:28:20 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/31 20:29:57 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h> 

int		ft_atoi(const char *str)
{
	int res;
	int neg;
	int start;
	
	neg = 0;
	res = 0;
	start = 0;
	while (str[start] == ' ')
		start++;
	if (str[start] == '-')
		neg = 1;
	if (str[start] == '+' || str[start] == '-')
		start = start + 1;
	while (str[start] >= '0' && str[start] <= '9')
		res = (res * 10) + str[start++] - '0';
	if (neg == 1)
		res = -res;
	return (res);
}

int		 main(int argc, char **argv)
{
	int a = ft_atoi(argv[1]);
	printf("%d\n", a);
	printf("%d\n", atoi(argv[1]));
}
