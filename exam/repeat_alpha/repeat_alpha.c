/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   repeat_alpha.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/30 11:21:36 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 11:34:16 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		main(int argc, char **argv)
{
	int i;
	int times;

	times = 0;
	i = 0;
	while (argv[1][i])
	{
		times = 0;
		if (argv[1][i] >= 'a' && argv[1][i] <= 'z')
		{
			while (times < 1 + (argv[1][i] - 'a'))
			{
				ft_putchar(argv[1][i]);
				times++;
			}
		}
		else if (argv[1][i] >= 'A' && argv[1][i] <= 'Z')
		{
			while (times < 1 + (argv[1][i] - 'A'))
			{
				ft_putchar(argv[1][i]);
				times++;
			}
		}
		i++;
	}
	return (0);
}
