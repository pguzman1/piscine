/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/24 14:50:20 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/24 15:10:49 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdlib.h>
#include <stdio.h>

int		ft_strlen(char *str)
{	
	int i;
	
	i = 0;
	while (str[i])
	{
		i++;
	}
	return (i);
}
/*
char	*ft_strcat(char *s1, const char *s2)
{
	int		len;
	int		i;

	len = ft_strlen(s1);
	i = 0;
	while (s2[i] != '\0')
	{
		s1[len + i] = s2[i];
		i++;
	}
	s1[len + i] = '\0';
	return (s1);
}
*/
char	*ft_strcat(char *dest, char *src)
{
	int	i;
	int	dest_size;

	i = 0;
	dest_size = 0;
	while (dest[dest_size])
		dest_size++;
	while (src[i])
	{
		dest[dest_size] = src[i];
		dest_size++;
		i++;
	}
	dest[dest_size] = '\0';
	return (dest);
}

int		main(void)
{
	
	char s1[999];
	s1[0] = 'h';
	s1[1] = 'o';
	char *s2 = "pato";
	char *s3;
	s3 = ft_strcat(s1, s2);
	printf("%s\n", s3);
	return (0);
}
