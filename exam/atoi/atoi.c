/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   atoi.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/20 07:55:35 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/31 14:50:02 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		atoi(char *str)
{
	int res;
	int i;
	
	n = (str[0] = '-') ? 1 : 0;
	i = n;
	while (str[i] == ' ')
		i++;
	while (str[i])
	{
		res = (res * 10) + str[i] - '0';
		i++;
	}
	if (n == 1)
		return (-res);
	return (res);
}

int		main(void)
{
	char *a;
	int b;
	
	a = "1234124";
	b = atoi(a);
	printf("%d\n", b);
}
