/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_str.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/29 10:40:45 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/29 10:50:58 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

int		main(int argc, char **argv)
{
	int i;
	char *str;

	i = 0;
	str = argv[1];
	while (str[i] == ' ')
		i++;
	while (str[i])
	{
		if (str[i] == ' ' && str[i - 1] != ' ')
		{
			ft_putchar(str[i]);
			ft_putchar(str[i]);
			ft_putchar(str[i]);
		}
		else if (str[i] != ' ')
			ft_putchar(str[i]);
		i++;
	}
}
