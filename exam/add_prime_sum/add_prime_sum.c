/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_prime_sum.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/28 09:27:09 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/28 11:29:21 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int n)
{
	if (n > 9)
	{
		ft_putnbr(n / 10);
		ft_putnbr(n % 10);
	}
	else if (n >= 0 && n < 10)
	{
		ft_putchar((char)n);
	}
}


int		ft_atoi(char *str)
{
	int i;
	int res;

	i = 0;
	res = 0;
	while (str[i])
	{
		res = (res * 10) + str[i] - '0';
		i++;
	}
	return (res);
}

int		ft_is_positivenumber(char *str)
{

	if ( str[0] < '0' && str[0] > '9')
	{
		return (0);
	}
	return (1);
}
int		main(int argc, char** argv)
{
	int i;
	int sum;

	i = 0;
	if (argc != 2 || (ft_is_positivenumber(argv[1]) == 0))
	{
		ft_putchar('0');
		ft_putchar('\n');
		return (0);
	}
	sum = ft_atoi(argv[1]);
	i = sum;
	while (i != 0)
	{
		i--;
		sum = sum;
	}
	ft_putnbr(sum);
	ft_putchar('\n');
	return (0);
}

