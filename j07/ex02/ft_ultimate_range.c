/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/17 13:55:46 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/17 22:30:36 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		*ft_ultimate_range(int **range, int min, int max)
{
	int	i;
	int *array;

	i = 0;
	if (min >= max)
		return (0);
	array = (int*)malloc(sizeof(*array) * (max - min) + 1);
	while (i < (max - min))
	{
		array[i] = min + i;
		i++;
	}
	array[i] = '\0';
	*range = array;
	return (max - min);
}
