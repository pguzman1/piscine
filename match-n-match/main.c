/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/11 10:26:08 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/14 08:25:51 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str);
int		ft_sqrt(int index);
int		ft_is_prime(int nb);
int		ft_iterative_factorial(int nb);
int		ft_sqrt(int nb);
void	ft_putchar(char a);
int		ft_putnbr(int a);
int		ft_find_next_prime(int nb);
int		match(char *s1, char *s2);
int		main(void)
{
	int a;
	char *b;
	char *i;

	b = "hola cabro";
	i = "hola cabro";
	a = match(b, i);
	ft_putnbr(a);
}
