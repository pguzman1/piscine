/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_bits.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/30 12:19:08 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/31 17:27:58 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{	
	write(1, &c, 1);	
}

void	print_bits(unsigned char c)
{
	char temp[9];
	int i;
	int j;

	i = 0;	
	while(c > 0)
		temp[i++] = (c % 2 == 0) ? '0' : '1', c /= 2;
	j = i;
	while (j <= 7)
		temp[j++] = '0';
	temp[j] = '\0';
	while (j >= 0)
		ft_putchar(temp[j--]);
}

int main(void)
{
	print_bits(1);
return (0);
}
