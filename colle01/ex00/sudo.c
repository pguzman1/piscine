/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sudo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/18 20:20:45 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/28 10:55:50 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

int ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		i++;
	}
	return (i);
}


int		ft_column(int **grid, int n, int column)
{
	int i;

	i = 0;
	while (i < 9)
	{
		if (n == grid[i][column])
		{
			return (1);
		}
		i++;
	}
	return (0);
}

// return 1 if n is found
// return 0 if n is not found
int		ft_row(int **grid, int n, int row)
{
	int i;

	i = 0;
	while (i < 9)
	{
		if (n == grid[row][i])
		{
			return (1);
		}
		i++;
	}
	return (0);
}
// return 1 if n is found
// return 0 if n is not found
int		ft_cell(int **grid, int n, int column, int row)
{
	int topleftrow;
	int	topleftcolumn;
	int i;
	int j;

	topleftrow = (row / 3) * 3;
	topleftcolumn = (column / 3) * 3;
	i = 0;
	j = 0;
	while (i < 3)
	{
		while (j < 3)
		{
			if (n == grid[topleftrow + i][topleftcolumn + j])
			{
				return (1);
			}
		}
		i++;
	}
	return (0);
}


void	ft_putnumber(int **grid, int n, int column, int row)
{
	grid[row][column] = n;
}

void	ft_printgrid(int **grid, int argc)
{
	int i;
	int j;

	i = 0;
	while (i < 9)
	{
		j = 0;
		while (j < 9)
		{
			ft_putchar((char)grid[i][j]);
			ft_putchar(' ');
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}
// return 1 if n is not  found
// return 0 if n is found
int		ft_isavailable(int **grid, int n, int column, int row)
{
	int col;
	int ro;
	int cell;

	col = ft_column(grid, n, column);
	ro = ft_row(grid, n, row);
	cell = ft_cell(grid, n, column, row);
	if (row == 0 && column == 0 && cell == 0)
	{
		return (1);
	}
	return (0); 
}


void 	ft_lastcandidate(int **grid, int column, int row)
{
	int i;
	int sum;
	char c;
	int number;
	
	sum = 0;
	i = 1;

	while (i <= 9)
	{
		sum = sum + ft_isavailable(grid, i, column, row);
		write(1,"\nafte", 4);
		if (ft_isavailable(grid, i, column, row) == 1)
		{
			number = i;
		}
		i++;
	}
	if (sum == 1)
	{
		c = number;
		ft_putnumber(grid, c, column, row);
	}
}



void	ft_walk(int **grid)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while(i < 9)
	{
		while (j < 9)
		{
				ft_lastcandidate(grid, i, j);
				write(1,"a", 1);
			j++;
		}
		i++;
	}
}

//check all numbers and if there is just one last candidate, it will change 
// the char
// return 1 if n is found
// return 0 if n is not found
int		main(int argc, char **argv)
{
	int **grid;
	int i;
	int j;
	int k;
	char a, c;
	
	k = 0;
	grid = (int **)malloc(sizeof(int *) * 9);
	while (k < 9)
	{
		grid[k] = (int*)malloc(9 * sizeof(int));
		k++;
	}
	i = 0;
	j = 0;
	while (i < 9)
	{	
		j = 0;
		while (j < 9)
		{
			if (argv[i + 1][j] == '.')
			{
				argv[i + 1][j] = '0';
			}
			grid[i][j] = (int)argv[i + 1][j];
			j++;
		}
		i++;
	}
	ft_printgrid(grid, 9);
	ft_walk(grid);
		ft_printgrid(grid, 9);
}
