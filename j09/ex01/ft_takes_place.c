/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_takes_place.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/16 18:04:26 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/16 23:22:02 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_takes_place(int hour)
{
	int pm;

	pm = 0;
	if (hour > 12 && hour != 24)
	{
		hour = hour % 12;
		pm = 1;
	}
	printf("%s", "THE FOLLOWING TAKES PLACE BETWEEN ");
	if (hour == 12)
		printf("%d.00 P.M. AND %d.00 P.M.", 12, 1);
	else if (hour == 24 || hour == 0)
		printf("%d.00 A.M. AND %d.00 A.M.", 12, 1);
	else if (pm == 1 && hour != 11)
		printf("%d.00 P.M. AND %d.00 P.M.", hour, hour + 1);
	else if (pm == 1 && hour == 11)
		printf("%d.00 P.M. AND %d.00 A.M.", hour, 12);
	else if (pm == 0 && hour != 11)
		printf("%d.00 A.M. AND %d.00 A.M.", hour, hour + 1);
	else if (pm == 0 && hour == 11)
		printf("%d.00 A.M. AND %d.00 P.M.", 11, 12);
	printf("%s\n", "");
}
