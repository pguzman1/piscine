/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rot42.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/16 21:58:07 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/17 13:16:42 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_rot42(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		if (str[i] >= 'a' && str[i] <= 'z')
		{
			if (str[i] <= 'j')
				str[i] = str[i] + 16;
			else if (str[i] > 'j')
				str[i] = str[i] - 10;
		}
		if (str[i] >= 'A' && str[i] <= 'Z')
		{
			if (str[i] <= 'J')
				str[i] = str[i] + 16;
			else if (str[i] > 'J')
				str[i] = str[i] - 10;
		}
		i++;
	}
	return (str);
}
