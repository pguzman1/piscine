/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unmatch.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/17 12:35:21 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/17 13:21:16 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_unmatch(int *tab, int length)
{
	int i;
	int j;
	int npair;

	i = 0;
	while (i < length)
	{
		j = 0;
		npair = 0;
		while (j < length)
		{
			if (tab[i] - tab[j] == 0 && i != j)
			{
				npair++;
			}
			if ((j == length - 1) && (npair % 2 == 0))
			{
				return (tab[i]);
			}
			j++;
		}
		i++;
	}
	return (0);
}
