/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_collatz_conjecture.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/17 07:50:00 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/17 13:22:28 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_collatz_conjecture(unsigned int base)
{
	if (base == 1)
	{
		return (1);
	}
	else if (base % 2 == 0)
	{
		base = base / 2;
	}
	else
	{
		base = 3 * base + 1;
	}
	return (ft_collatz_conjecture(base));
}
