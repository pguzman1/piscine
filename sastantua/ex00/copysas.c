/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sastantua.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/11 23:47:01 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/12 01:27:06 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);
/*
void	sastantua()
{
	
}

void	build_floor(int number_floor, number_sas)
{
	ft_build_line();
}
*/
void	ft_build_line(int space, int length)
{
	while (space > 0)
	{
		ft_putchar(' ');
		space--;
	}
	ft_putchar('/');
	while (length > 0)
	{
		ft_putchar('*');
		length--;
	}
	ft_putchar('\\');
	ft_putchar('\n');
}
