/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_checkcolle.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/25 16:16:10 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/28 11:57:12 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_colle.h"

int		ft_iscolle00(char *buf, int length, int width)
{
	if (length == 1 && width == 1 && buf[0] == 'o')
		return (0);
	else if (length == 1 && buf[0] == 'o' && buf[((width - 1) * 2)] == 'o')
		return (0);
	else if (buf[length - 1] == 'o' && width == 1)
		return (0);
	else if (buf[length - 1] == 'o' && buf[((length + 1) * (width - 1))] == 'o')
		return (0);
	else
		return (-1);
}

int		ft_iscolle01(char *buf, int length, int width)
{
	if (length == 1 && width == 1 && buf[0] == '/')
		return (1);
	else if (length == 1 && buf[0] == '/' && buf[((width - 1) * 2)] == '\\')
		return (1);
	else if (buf[length - 1] == '\\' && width == 1)
		return (1);
	else if (buf[length - 1] == '/' &&\
			buf[((length + 1) * (width - 1))] == '\\')
		return (1);
	else
		return (-1);
}

int		ft_iscolle02(char *buf, int length, int width)
{
	if (length == 1 && width == 1 && buf[0] == 'A')
		return (2);
	else if (length == 1 && buf[0] == 'A' && buf[((width - 1) * 2)] == 'C')
		return (2);
	else if (buf[length - 1] == 'A' && width == 1)
		return (2);
	else if (buf[length - 1] == 'A' && buf[((length + 1) * (width - 1))] == 'C')
		return (2);
	else
		return (-1);
}

int		ft_iscolle03(char *buf, int length, int width)
{
	if (length == 1 && width == 1 && buf[0] == 'A')
		return (3);
	else if (length == 1 && buf[0] == 'A' && buf[((width - 1) * 2)] == 'A')
		return (3);
	else if (buf[length - 1] == 'C' && width == 1)
		return (3);
	else if (buf[length - 1] == 'C' && buf[((length + 1) * (width - 1))] == 'A')
		return (3);
	else
		return (-1);
}

int		ft_iscolle04(char *buf, int length, int width)
{
	if (length == 1 && width == 1 && buf[0] == 'A')
		return (4);
	else if (length == 1 && buf[0] == 'A' && buf[((width - 1) * 2)] == 'C')
		return (4);
	else if (buf[length - 1] == 'C' && width == 1)
		return (4);
	else if (buf[length - 1] == 'C' && buf[((length + 1) * (width - 1))]\
			== 'C')
		return (4);
	else
		return (-1);
}
