/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mesure.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/25 16:17:35 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/28 11:59:29 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_colle.h"

int		ft_length(char *buf)
{
	int i;

	i = 0;
	while (buf[i] != '\n')
		i++;
	return (i);
}

int		ft_width(char *buf)
{
	int i;
	int j;

	i = 0;
	j = 0;
	while (buf[i] != '\0')
	{
		if (buf[i] == '\n')
			j++;
		i++;
	}
	return (j);
}

void	ft_print(int colle, int length, int width)
{
	ft_putstr("[colle-0");
	ft_putnbr(colle);
	ft_putstr("] [");
	ft_putnbr(length);
	ft_putstr("] [");
	ft_putnbr(width);
	ft_putstr("]");
}

void	ft_printpipes(void)
{
	ft_putstr(" || ");
}

void	ft_checkcolle(char *str, int length, int width)
{
	int i[5];

	i[0] = ft_iscolle00(str, width, length);
	if (i[0] == 0)
		ft_print(0, length, width);
	i[1] = ft_iscolle01(str, length, width);
	if (i[1] == 1)
		ft_print(1, length, width);
	i[2] = ft_iscolle02(str, length, width);
	if (i[2] == 2)
		ft_print(2, length, width);
	i[3] = ft_iscolle03(str, length, width);
	if (i[2] != -1)
		ft_printpipes();
	if (i[3] == 3)
		ft_print(3, length, width);
	i[4] = ft_iscolle04(str, length, width);
	if (i[3] != -1 && i[4] == 4 && i[2] != i[3])
		ft_printpipes();
	if (i[4] == 4)
		ft_print(4, length, width);
}
