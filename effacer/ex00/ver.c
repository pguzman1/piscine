/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ver.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/26 18:08:54 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/26 22:11:25 by gseropia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_colle.h"

int		ft_isvalid(char *buf, int length, int width)
{
	int i;

	i = 0;
	if (length == 0 || width == 0)
		return (0);
	while (buf[i])
	{
		if (buf[i] != 'o' || buf[i] != '/' || buf[i] != '\\' || buf[i] != '-' \
			|| buf[i] != '|' || buf[i] != 'A' || buf[i] != 'B' \
			|| buf[i] != 'C' || buf[i] != '*' || buf[i] != ' ' \
			|| buf[i] != '\n')
			return (0);
		i++;
	}
	return (1);
}

int		ft_isvalid2(char *buf, int length, int width)
{
	int i;

	i = 2;
	while (i < length - 1)
	{
		if (buf[i] != buf[1])
			return (0);
		if (buf[((length + 1) * (width - 1))] !=\
			buf[((length + 1) * (width - 1)) + i])
			return (0);
		i++;
	}
	return (1);
}
