/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/23 11:24:50 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/26 21:18:32 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "ft_colle.h"

int		main(int argc, char **argv)
{
	int		ret;
	char	buf[BUF_SIZE + 1];
	int		width;
	int		length;
	
	while ((ret = read(0, buf, BUF_SIZE)))
	{
		buf[ret] = '\0';
		length = ft_length(buf);
		width = ft_width(buf);
		if (ft_isvalid(buf, length, width) == 1)
		{		
			ft_checkcolle(buf, length, width);
			ft_putchar('\n');
		}
		else
		{
			ft_putstr(aucune);
			ft_putchar('\n');
			return (0);
		}
	}	
	ft_putstr("aucune\n");
	return (0);
}
