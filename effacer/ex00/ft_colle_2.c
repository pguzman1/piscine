/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_colle-2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gseropia <gseropia@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/26 21:39:05 by gseropia          #+#    #+#             */
/*   Updated: 2015/07/26 22:45:13 by gseropia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_colle.h"

int		main(int argc, char **argv)
{
	int		ret;
	char	buf[BUF_SIZE + 1];
	int		width;
	int		length;

	while ((ret = read(0, buf, BUF_SIZE)))
	{
		buf[ret] = '\0';
		length = ft_length(buf);
		width = ft_width(buf);
		ft_checkcolle(buf, length, width);
		ft_putchar('\n');
		return (0);
	}
	ft_putstr("aucune\n");
	return (0);
}
