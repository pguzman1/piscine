/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_colle.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/26 14:47:38 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/26 22:46:09 by gseropia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_COLLE_H
# define FT_COLLE_H
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>
# define BUF_SIZE 5000

void	ft_putchar(char a);
void	ft_putnbr(int n);
void	ft_putstr(char *str);
void	ft_print(int colle, int length, int width);
int		ft_length(char *buf);
int		ft_width(char *buf);
void	ft_checkcolle(char *str, int length, int width);
int		ft_iscolle00(char *buf, int length, int width);
int		ft_iscolle01(char *buf, int length, int width);
int		ft_iscolle02(char *buf, int length, int width);
int		ft_iscolle03(char *buf, int length, int width);
int		ft_iscolle04(char *buf, int length, int width);
#endif
