/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_front.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/22 11:36:29 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/22 22:46:13 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "ft_list.h"
#include <unistd.h>
#include <stdio.h>

void	ft_list_push_front(t_list **begin_list, void *data)
{
	t_list *list;

	list = ft_create_elem(data);
	list->next = *begin_list;
	*begin_list = list;
}

int		main(void)
{
	t_list *list;
	list = NULL;
	ft_list_push_front(&list, "hola");
	ft_list_push_front(&list, "hojo");
	ft_list_push_front(&list, "hoik");
	ft_list_push_front(&list, "pepe");
	printf("%d\n",ft_list_size(list));
	ft_printlist(list);
}
