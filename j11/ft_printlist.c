/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printlist.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/22 14:39:18 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/23 14:56:09 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "ft_list.h"

void	ft_putchar(char a)
{
	write(1, &a, 1);
}

void	ft_putstr(char *a)
{
	int i;

	i = 0;
	while(a[i])
	{
		ft_putchar(a[i]);
		i++;
	}
}

void	ft_printlist(t_list *list)
{
		while (list)
		{
			ft_putstr(list->data);
			list = list->next;
		}
		else
			ft_putstr(list);
}
