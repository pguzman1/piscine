/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_clear.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/22 18:00:13 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/23 15:49:34 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_clear(t_list **begin_list)
{
	t_list *list;
	t_list *temp;

	list = *begin_list;
	while (list)
	{
		if (list->next)
			temp = list->next;
		else
			temp = NULL;
		free(list);
		list = temp;
	}
}
