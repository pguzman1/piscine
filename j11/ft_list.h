/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/22 09:31:21 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/23 14:37:44 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
typedef struct		s_list
{
	struct s_list	*next;
	void			*data;
}					t_list;

t_list		*ft_create_elem(void *data)
{
	t_list *elem;
	elem = malloc(sizeof(t_list));
	if (elem)
	{
		elem->data = data;
		elem->next = NULL;
	}
	return (elem);
}

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

void	ft_list_push_back(t_list **begin_list, void *data)
{
	t_list *temp;
	temp = *begin_list;
	if (temp)
		{
			while(temp)
			{
				temp = temp->next;
			}
			temp->next = ft_create_elem(data);
		}
	else
		*begin_list = ft_create_elem(data);
}

void	ft_printlist(t_list *list)
{
		while (list)
		{
			ft_putstr(list->data);
			list = list->next;
		}
}

void	ft_list_push_front(t_list **begin_list, void *data)
{
	t_list *list;

	list = ft_create_elem(data);
	list->next = *begin_list;
	*begin_list = list;
}

int		ft_list_size(t_list *begin_list)
{
	int		i;
	t_list *temp;

	temp = begin_list;
	i = 0;
	if (begin_list != NULL)
	{
		i = 1;
		while (temp->next)
		{
			temp = temp->next;
			i++;
		}
		return (i);
	}
	else
	{
		return (0);
	}
}
