/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/22 16:53:19 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/23 14:55:18 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"
#include <unistd.h>
t_list	*ft_list_push_params(int ac, char **av)
{
	int		i;
	t_list	*temp;

	temp = 0;
	i = 1;
	while (ac > i)
	{
		ft_list_push_front(&temp, av[i]);
		i++;
	}
	return (temp);	
}

int		main(int argc, char **argv)
{
	t_list *list;

	list = ft_list_push_params(argc, argv);
	ft_printlist(list);
}
