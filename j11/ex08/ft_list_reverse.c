/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_reverse.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/23 08:24:49 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/23 18:55:10 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_reverse(t_list **begin_list)
{
	t_list *temp;
	t_list *temp2;
	t_list *list;

	list = *begin_list;
	temp = list->next;
	temp2 = temp->next;
	list->next = 0;
	temp->next = list;
	while (temp2)
	{
		list = temp;
		temp = temp2;
		temp2 = temp2->next;
		temp->next = list;
	}
}
