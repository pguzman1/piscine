/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_size.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/22 12:13:54 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/23 18:54:30 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

int		ft_list_size(t_list *begin_list)
{
	int		i;
	t_list	*temp;

	temp = begin_list;
	i = 0;
	if (begin_list)
	{
		while (temp)
		{
			temp = temp->next;
			i++;
		}
		return (i);
	}
	else
	{
		return (0);
	}
}
