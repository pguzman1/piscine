/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_params.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/22 16:53:19 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/23 15:11:38 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_push_front(t_list **begin_list, void *data)
{
	t_list *list;

	list = *begin_list;
	if (*begin_list)
	{
		list = ft_create_elem(data);
		list->next = *begin_list;
		*begin_list = list;
	}
	else
	{
		*begin_list = ft_create_elem(data);
	}
}

t_list	*ft_list_push_params(int ac, char **av)
{
	int		i;
	t_list	*temp;

	temp = 0;
	i = 1;
	while (ac > i)
	{
		ft_list_push_front(&temp, av[i]);
		i++;
	}
	return (temp);
}
