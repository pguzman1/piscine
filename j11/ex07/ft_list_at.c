/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_at.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/22 21:35:21 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/23 22:34:02 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

int		ft_list_size(t_list *begin_list)
{
	int		i;
	t_list	*temp;

	temp = begin_list;
	i = 0;
	if (begin_list)
	{
		while (temp->next)
		{
			temp = temp->next;
			i++;
		}
		return (i);
	}
	else
	{
		return (0);
	}
}

t_list	*ft_list_at(t_list *begin_list, unsigned int nbr)
{
	t_list	*list;
	t_list	*temp;
	int		i;

	i = 0;
	list = begin_list;
	temp = list;
	if (nbr > ft_list_size(list))
	{
		return (0);
	}
	if (i < nbr)
	{
		while (i < nbr)
		{
			temp = temp->next;
			i++;
		}
		return (temp);
	}
	else
	{
		return (0);
	}
}
