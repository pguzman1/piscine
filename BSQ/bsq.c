/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/28 11:40:46 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 14:15:40 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		main(int argc, char **argv)
{
	int		ret;
	char	buf[BUF_SIZE + 1];
	int		i;
	char	test = 'h';

	i = 0;
	while ((ret = read(0, buf, BUF_SIZE)))
	{
		buf[ret] = '\0';
		while (buf[i] != '\n')
			i++;
	//	ft_putstr(buf);
	}
}
