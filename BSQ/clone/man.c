/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   man.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/30 14:32:08 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 14:32:51 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

int		size2d(char *str);
char	**array_2d(char *str);

char	ft_putchar(char c)
{
	return (write(1, &c, 1));
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

char	create_array(char *file)
{
	char	**grid;
	char	buf[30000];
	int		fd;
	int		ret;
	int		i;

	i = 0;
	fd = open(file, O_RDONLY);
	while ((ret = read(fd, buf, 30000)))
	{
		buf[ret] = '\0';
	}
	grid = array_2d(buf);
	while (i < 20)
		ft_putchar(grid[1][i++]);
	return (0);
}

int		main(int ac, char **av)
{
	(ac != 1) ? create_array(av[1]) : 0;
	return (0);
}
