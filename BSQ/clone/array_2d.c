/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   array_2d.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/28 14:19:08 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 14:30:26 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		size2d(char *str)
{
	int i;

	while (str[i])
	{
		if (str[i] == '\n')
		{
			break ;
		}
		i++;
	}
	return (i);
}

char	**array_2d(char *str)
{
	int		k;
	int		i;
	int		j;
	char	**grid;

	k = 0;
	grid = (char **)malloc(sizeof(char *) * (size2d(str) + 1));
	while (k <= size2d(str))
		grid[k] = (char*)malloc((size2d(str) + 1) * sizeof(char)), k++;
	i = 1;
	k = 0;
	while (k <= size2d(str))
		grid[0][k++] = '0';
	while (i < size2d(str) + 1)
	{
		j = 1;
		while (j < size2d(str))
		{
			if (str[(i - 1) * size2d(str) + j] == '\n')
				break ;
			grid[i][j] = str[(i - 1) * size2d(str) + j], j++;
		}
		i++;
	}
	return (grid);
}
