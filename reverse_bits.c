/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse_bits.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/30 14:05:41 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/31 19:01:25 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>


unsigned char mv(unsigned char a, int i)
{
	unsigned char t;
	
	t = 0;
	t = a << i;
	t = t >> 7;
	t = t << i;
	return (t);
}
unsigned char reverse_bits(unsigned char octet)
{
	unsigned char a;
	int i;

	i = 7;
	a = 0;
	while (i >= 0)
	{
		a = a + mv(octet, i);
		i--;
	}
	return (a);
}


int		main(void)
{
	unsigned char a = 234;
	unsigned char b = reverse_bits(a);
	printf("%u\n", b);
}
