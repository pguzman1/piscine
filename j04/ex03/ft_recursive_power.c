/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/13 13:56:26 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/14 13:24:34 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_recursive_power(int nb, int power)
{
	int temp1;
	int temp;

	temp1 = nb;
	if (power > 1)
	{
		temp1 = temp1 * ft_recursive_power(nb, power - 1);
		return (temp1);
	}
	else if (power == 1)
	{
		return (nb);
	}
	else if (nb != 0 && power == 0)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}
