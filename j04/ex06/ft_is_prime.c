/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/13 17:19:44 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/13 18:41:23 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_prime(int nb)
{
	int counter;

	counter = 2;
	if (nb < 2)
		return (0);
	while (counter - 1 <= nb / 2)
	{
		if (nb != 2 && nb % counter == 0)
			return (0);
		if (nb == 2)
			return (1);
		counter++;
	}
	return (1);
}
