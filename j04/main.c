/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/11 10:26:08 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/14 13:23:38 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str);
int		ft_sqrt(int index);
int		ft_is_prime(int nb);
int		ft_iterative_power(int nb, int power);
int		ft_iterative_factorial(int nb);
int		ft_recursive_factorial(int nb);
int		ft_recursive_power(int nb, int power);
int		ft_sqrt(int nb);
void	ft_putchar(char a);
int		ft_putnbr(int a);
int		ft_find_next_prime(int nb);
int		main(void)
{
//	char a;
	int b;
	int i;
	b = 4;
	i = ft_recursive_power(1, 0);
	ft_putnbr(i);
}
