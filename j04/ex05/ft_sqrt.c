/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/13 17:07:10 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/14 12:36:04 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	int counter;

	counter = 1;
	while (counter <= nb / 2)
	{
		if (counter * counter == nb)
		{
			return (counter);
		}
		counter++;
	}
	if (nb == 1)
	{
		return (1);
	}
	return (0);
}
