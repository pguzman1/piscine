/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fibonacci.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/13 14:03:13 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/14 19:32:54 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_fibonacci(int index)
{
	int temp;

	temp = 0;
	if (index > 1)
	{
		temp = ft_fibonacci(index - 1) + ft_fibonacci(index - 2);
		return (temp);
	}
	else if (index == 1)
	{
		return (1);
	}
	else if (index == 0)
	{
		return (0);
	}
	else
	{
		return (-1);
	}
}
