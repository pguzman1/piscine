/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/13 12:24:21 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/15 16:11:44 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial(int nb)
{
	int temp1;
	int temp;

	if (nb > 0 && nb <= 12)
	{
		temp1 = nb;
		while (nb > 1)
		{
			temp = nb;
			temp1 = temp1 * (temp - 1);
			nb--;
		}
		return (temp1);
	}
	else if (nb == 0)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}
