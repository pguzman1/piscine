/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/13 12:50:41 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/14 14:40:42 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_power(int nb, int power)
{
	int temp1;
	int temp;

	temp1 = nb;
	if (power > 0)
	{
		while (power > 1)
		{
			temp = nb;
			temp1 = temp1 * temp;
			power--;
		}
		return (temp1);
	}
	else if (nb != 0 && power == 0)
	{
		return (1);
	}
	else
	{
		return (0);
	}
}
