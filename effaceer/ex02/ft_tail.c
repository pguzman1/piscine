/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tail.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/23 22:40:53 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/24 23:24:32 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#define BUF_SIZE 30000
#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i])
	{
		ft_putchar(str[i]);
		i++;
	}
}

int		ft_atoi(char *str)
{
	int	i;
	int	res;

	res = 0;
	i = 0;
	while (str[i])
	{
		res = (res * 10) + str[i] - '0';
		i++;
	}
	return (res);
}

void	ft_pro(int a, int argc)
{
	if (a != argc - 1)
	ft_putchar('\n');
}

void	ft_printerror(char *str)
{
	ft_putstr("tail: ");
	ft_putstr(str);
	ft_putstr(": No such file or directory\n");
}

void	ft_printnamefile(char *str)
{
	ft_putstr("==> ");
	ft_putstr(str);
	ft_putstr(" <==");
	ft_putchar('\n');
}

int		main(int argc, char **argv)
{
	char	buf[BUF_SIZE + 1];
	int		i[2];
	int		index[3];

	i[0] = 2;
	while (i[0] < argc)
	{
		index[1] = open(argv[i[0]], O_RDONLY);
		if (index[1] != -1)
		{
			index[2] = read(index[1], buf, BUF_SIZE);
			close(index[1]);
			index[1] = open(argv[i[0]], O_RDONLY);
			i[1] = 0;
			if (argc > 3)
				ft_printnamefile(argv[i[0]]);
			while ((index[0] = read(index[1], buf, 1)))
			{
				buf[index[1]] = '\0';
				if (i[1] >= index[2] - ft_atoi(argv[1]))
					ft_putstr(buf);
				i[1]++;
			}
			ft_pro(i[0], argc);
			close(index[1]);
		}
		else
			ft_printerror(argv[i[0]]);
		i[0]++;
	}
}
