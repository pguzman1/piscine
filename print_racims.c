/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_racims.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pguzman <pguzman@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/07/30 14:35:50 by pguzman           #+#    #+#             */
/*   Updated: 2015/07/30 14:49:51 by pguzman          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

void	print_racims(unsigned char octet)
{
	char array[9];
	int i;
	int j;

	i = 0;
	while (octet > 0)
		array[i++] = (octet % 2 == 0) ? '0' : '1', octet /= 2;
	j = i;
	while (j <= 7)
		ft_putchar('0'), j++;
	while (i >= 0)
		ft_putchar(array[i--]);
	ft_putchar('\n');
}

int 	main(void)
{
	print_racims(13);\
	return (0);
}
